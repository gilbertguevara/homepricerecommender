package com.hugeinc.recommender.util;

import com.hugeinc.recommender.model.Home;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 4:36 PM
 */
public final class HomeParser implements Parser<Home> {
    private static final Logger LOG = LoggerFactory.getLogger(HomeParser.class);
    private static final String FIELD_SEPARATOR = "\\|";

    public final Home parse(String line) {
        String[] fields = line.split(FIELD_SEPARATOR);
        Home home = new Home();

        try {
            home.setMlsNumber(Double.valueOf(fields[0]));
            home.setCity(fields[1]);
            home.setSquaredFeet(Double.valueOf(fields[2]));
            home.setBedrooms(Double.valueOf(fields[3]));
            home.setBathrooms(Double.valueOf(fields[4]));
            home.setGarages(Double.valueOf(fields[5]));
            home.setAge(Double.valueOf(fields[6]));
            home.setAcres(Double.valueOf(fields[7]));
            home.setPrice(Double.valueOf(fields[8]));
        } catch (IllegalArgumentException e) {
            LOG.error("Error parsing line: " + line, e);
        }

        return home;
    }
}
