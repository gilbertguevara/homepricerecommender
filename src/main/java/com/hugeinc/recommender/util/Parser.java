package com.hugeinc.recommender.util;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 4:48 PM
 */
public interface Parser<T> {
    /**
     * Parse string data into the given object type
     * @param line
     * @return
     */
    T parse(String line);
}
