package com.hugeinc.recommender;

import com.hugeinc.recommender.model.Home;
import com.hugeinc.recommender.service.HomePriceRecommenderService;
import com.hugeinc.recommender.service.RecommenderService;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.feature.StandardScalerModel;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 8:33 PM
 */
public class HomePriceRecommenderApp {

    public static void main(String[] args) {
        // Reads the file from hdfs and creates the resilient data
        RecommenderService<Home> recommenderService = new HomePriceRecommenderService();
        JavaRDD<Home> homeData = recommenderService.initializeData();


        MultivariateStatisticalSummary summary = recommenderService.getStatistics(homeData);
        System.out.println("Price mean: " + summary.mean());
        System.out.println("Price max: " + summary.max());
        System.out.println("Price min: " + summary.min());

        // Filter out non standard data
        JavaRDD<Home> filteredData = recommenderService.filterData(homeData);
        // See correlation between square feet and price
        double correlation = recommenderService.getCorrelationStatistics(filteredData);
        System.out.println("Living area and price correlation: " + correlation);

        // Scale data to standardize features before using it
        StandardScalerModel scalerModel = recommenderService.buildScalerModel(filteredData);
        System.out.println("Scaler model mean: " + scalerModel.mean().toArray()[0]);
        System.out.println("Scaler model variance: " + scalerModel.std().toArray()[0]);

        // Get standardized features to run the model
        JavaRDD<LabeledPoint> scaledData = recommenderService.getScaledData(filteredData, scalerModel);
        // Build the model, running the linear regression over the scaled data features
        LinearRegressionModel model = recommenderService.buildModel(scaledData);
        // Calculates Mean Squared Error
        double mse = recommenderService.getMSE(scaledData, model);
        System.out.println("MSE: " + mse);
        // Persist the models into hdfs
        recommenderService.persistModelsToHdfs(scalerModel, model);

    }
}
