package com.hugeinc.recommender;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.feature.StandardScalerModel;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LinearRegressionModel;

/**
 * User: HUGE-gilbert
 * Date: 12/21/15
 * Time: 9:58 AM
 */
public class HomePriceClient {
    public static void main(String[] args) {
        JavaSparkContext sparkContext = new JavaSparkContext(new SparkConf()
                .setMaster("local[1]").setAppName("Home Price Client"));

        JavaRDD<LinearRegressionModel> model = sparkContext.objectFile("hdfs://localhost:9000/user/gilbert/linearRegression.model");
        JavaRDD<StandardScalerModel> scalerModel = sparkContext.objectFile("hdfs://localhost:9000/user/gilbert/scaler.model");

        // age, bathrooms, bedrooms, garage, square feet
        double age = 11.0;
        double bathrooms = 2.0;
        double bedrooms = 2.0;
        double garages = 1.0;
        double squareFeet = 2200.0;

        StringBuilder sb = new StringBuilder("Predicting value for: Age = ").append(age).append(", Bathrooms = ");
        sb.append(bathrooms).append(", Bedrooms = ").append(bedrooms).append(", Garage = ").append(garages);
        sb.append(", Living area = ").append(squareFeet);
        System.out.println(sb.toString());

        double predictedPrice = model.first().predict(scalerModel.first().transform(Vectors.dense(age, bathrooms, bedrooms, garages, squareFeet)));
        System.out.println("Predicted price: " + predictedPrice);

        sparkContext.stop();
    }
}
