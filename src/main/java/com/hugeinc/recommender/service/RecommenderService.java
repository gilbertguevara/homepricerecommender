package com.hugeinc.recommender.service;

import com.hugeinc.recommender.model.Home;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.feature.StandardScalerModel;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;

/**
 * User: HUGE-gilbert
 * Date: 12/21/15
 * Time: 9:14 AM
 */
public interface RecommenderService<T> {
    /**
     * Reads data from Hadoop and creates the resilient data in spark
     * @return
     */
    JavaRDD<T> initializeData();

    /**
     * Get statistics about the data
     * @param homeData
     * @return
     */
    MultivariateStatisticalSummary getStatistics(JavaRDD<T> homeData);

    /**
     * Filter data to remove not applicable rows
     * @param homeData
     * @return
     */
    JavaRDD<T> filterData(JavaRDD<T> homeData);

    /**
     * Get correlation statistics between a feature and the output
     * @param filteredData
     * @return
     */
    double getCorrelationStatistics(JavaRDD<T> filteredData);

    /**
     * Build the scaler model
     * @param filteredData
     * @return
     */
    StandardScalerModel buildScalerModel(JavaRDD<T> filteredData);

    /**
     * Get scaled data
     * @param filteredData
     * @param scalerModel
     * @return
     */
    JavaRDD<LabeledPoint> getScaledData(JavaRDD<T> filteredData, StandardScalerModel scalerModel);

    /**
     * Build linear regression model using the scaled data
     * @param scaledData
     * @return
     */
    LinearRegressionModel buildModel(JavaRDD<LabeledPoint> scaledData);

    /**
     * Get the Mean Squared Error of the predictions
     * @param scaledData
     * @param model
     * @return
     */
    double getMSE(JavaRDD<LabeledPoint> scaledData, LinearRegressionModel model);

    /**
     * Persists models to hadoop
     * @param scalerModel
     * @param model
     */
    void persistModelsToHdfs(StandardScalerModel scalerModel, LinearRegressionModel model);

    /**
     * Stop spark context
     */
    void finish();
}
