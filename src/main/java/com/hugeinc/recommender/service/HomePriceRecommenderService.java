package com.hugeinc.recommender.service;

import com.hugeinc.recommender.model.Home;
import com.hugeinc.recommender.model.PricePrediction;
import com.hugeinc.recommender.util.HomeParser;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.feature.StandardScaler;
import org.apache.spark.mllib.feature.StandardScalerModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.regression.LinearRegressionWithSGD;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 4:10 PM
 */
public class HomePriceRecommenderService implements RecommenderService<Home>, Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(HomePriceRecommenderService.class);
    private static final long serialVersionUID = 3334618644676703042L;

    private static final String HOME_DATA_FILE_PATH = "hdfs:///user/gilbert/homeprice.data";
    private static final int NUM_ITERATIONS = 1000;
    private static final double STEP_SIZE = 0.2;

    // Attributes
    private JavaSparkContext sparkContext;

    @Override
    public JavaRDD<Home> initializeData() {
        sparkContext = new JavaSparkContext(new SparkConf().setAppName("Home Price Recommender"));
        JavaRDD<String> homeFileData = sparkContext.textFile(HOME_DATA_FILE_PATH).cache();
        JavaRDD<Home> homeRDD = homeFileData.map(line -> new HomeParser().parse(line));
        return homeRDD;
    }

    @Override
    public MultivariateStatisticalSummary getStatistics(JavaRDD<Home> homeData) {
        JavaRDD<Vector> priceVector = homeData.map(home -> Vectors.dense(home.getPrice()));
        MultivariateStatisticalSummary summary = Statistics.colStats(priceVector.rdd());
        return summary;
    }

    @Override
    public JavaRDD<Home> filterData(JavaRDD<Home> homeData) {
        JavaRDD<Home> filteredData = homeData.filter(home ->
                (home.getPrice() > 100000.0 && home.getPrice() < 400000.0 && home.getSquaredFeet() > 1000.0)).cache();
        return filteredData;
    }

    @Override
    public double getCorrelationStatistics(JavaRDD<Home> filteredData) {
        double correlation = Statistics.corr(filteredData.map(
                home -> home.getPrice()), filteredData.map(home -> home.getSquaredFeet()));
        return correlation;
    }

    /**
     * Return the labeled data using age, bathrooms, bedrooms, garages, squared feed with price label
     *
     * @param filteredData
     * @return
     */
    private JavaRDD<LabeledPoint> getLabeledData(JavaRDD<Home> filteredData) {
        JavaRDD<LabeledPoint> labeledData = filteredData.map(home ->
                new LabeledPoint(home.getPrice(), Vectors.dense(home.getAge(), home.getBathrooms(), home.getBedrooms(),
                        home.getGarages(),
                        home.getSquaredFeet()))).cache();
        return labeledData;
    }

    @Override
    public StandardScalerModel buildScalerModel(JavaRDD<Home> filteredData) {
        JavaRDD<LabeledPoint> labeledData = getLabeledData(filteredData);

        // Scale features to 0 mean and common variance
        StandardScaler standardScaler = new StandardScaler(true, true);
        StandardScalerModel scalerModel = standardScaler.fit(labeledData.map(x -> x.features()).rdd());

        return scalerModel;
    }

    @Override
    public JavaRDD<LabeledPoint> getScaledData(JavaRDD<Home> filteredData, StandardScalerModel scalerModel) {
        JavaRDD<LabeledPoint> labeledData = getLabeledData(filteredData);

        JavaRDD<LabeledPoint> scaledData = labeledData.map(data ->
                new LabeledPoint(data.label(), scalerModel.transform(Vectors.dense(data.features().toArray()))));

        return scaledData;
    }

    @Override
    public LinearRegressionModel buildModel(JavaRDD<LabeledPoint> scaledData) {
        LinearRegressionWithSGD linearRegression = new LinearRegressionWithSGD();
        linearRegression.setIntercept(true);
        linearRegression.optimizer().setNumIterations(NUM_ITERATIONS).setStepSize(STEP_SIZE);

        return linearRegression.run(scaledData.rdd());
    }

    @Override
    public double getMSE(JavaRDD<LabeledPoint> scaledData, LinearRegressionModel model) {
        JavaRDD<PricePrediction> predictions = scaledData.map(point -> {
            double price = point.label();
            double prediction = model.predict(point.features());
            return new PricePrediction(price, prediction);
        });

        JavaRDD<Double> residualSquares = predictions.map(prediction ->
                Math.pow(prediction.getPrice() - prediction.getPrediction(), 2));
        double rss = residualSquares.reduce((a, b) -> a + b);
        System.out.println("RSS: " + rss);

        double mse = rss / residualSquares.count();
        return mse;
    }

    @Override
    public void persistModelsToHdfs(StandardScalerModel scalerModel, LinearRegressionModel model) {
        // Delete before persist
        try {
            FileSystem fileSystem = FileSystem.get(sparkContext.hadoopConfiguration());
            fileSystem.delete(new Path("/user/gilbert/linearRegression.model"), true);
            fileSystem.delete(new Path("/user/gilbert/scaler.model"), true);
        } catch (IOException e) {
           LOG.error("Error while removing path", e);
        }

        sparkContext.parallelize(Arrays.asList(model)).saveAsObjectFile("hdfs:///user/gilbert/linearRegression.model");
        sparkContext.parallelize(Arrays.asList(scalerModel))
                .saveAsObjectFile("hdfs:///user/gilbert/scaler.model");
    }

    @Override
    public void finish() {
        sparkContext.stop();
    }
}
