package com.hugeinc.recommender.model;

import java.io.Serializable;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 8:19 PM
 */
public class PricePrediction implements Serializable {
    private static final long serialVersionUID = 3314208189113604000L;

    private Double price;
    private Double prediction;

    public PricePrediction() {
    }

    public PricePrediction(Double price, Double prediction) {
        this.price = price;
        this.prediction = prediction;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrediction() {
        return prediction;
    }

    public void setPrediction(Double prediction) {
        this.prediction = prediction;
    }
}
