package com.hugeinc.recommender.model;

import java.io.Serializable;

/**
 * User: HUGE-gilbert
 * Date: 12/20/15
 * Time: 4:06 PM
 */
public class Home implements Serializable {
    private static final long serialVersionUID = 8934664582357276948L;

    private Double mlsNumber;
    private String city;
    private Double squaredFeet;
    private Double bedrooms;
    private Double bathrooms;
    private Double garages;
    private Double age;
    private Double acres;
    private Double price;

    public Double getMlsNumber() {
        return mlsNumber;
    }

    public void setMlsNumber(Double mlsNumber) {
        this.mlsNumber = mlsNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getSquaredFeet() {
        return squaredFeet;
    }

    public void setSquaredFeet(Double squaredFeet) {
        this.squaredFeet = squaredFeet;
    }

    public Double getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Double bedrooms) {
        this.bedrooms = bedrooms;
    }

    public Double getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(Double bathrooms) {
        this.bathrooms = bathrooms;
    }

    public Double getGarages() {
        return garages;
    }

    public void setGarages(Double garages) {
        this.garages = garages;
    }

    public Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public Double getAcres() {
        return acres;
    }

    public void setAcres(Double acres) {
        this.acres = acres;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
